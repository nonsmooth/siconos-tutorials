# The siconos tutorial

This repository contains all sources files and examples programs for Siconos software.

There you will find some tutorial (jupyter notebooks) 
and a list of examples for different areas of application.


Please 

- visit directory **examples** to find a simulation matching your field of interest or

- visit directory **siconos-notebooks** and 
try [![Binder](https://plmbinder.math.cnrs.fr/binder/badge_logo.svg)](https://plmbinder.math.cnrs.fr/binder/v2/git/https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr%2Fnonsmooth%2Fsiconos-tutorials.git/HEAD)
 to start an interactive python environment in which you can run siconos.
 
Back to [Siconos home page](https://nonsmooth.gricad-pages.univ-grenoble-alpes.fr/siconos/index.html)
